## Required Variables

These variables need to be set and exported
`export ZABBIX_SERVER_LDAP_BIND_PASSWORD`
`export ZABBIX_SERVER_DBPASSWORD`

To run in OVH:

```bash
export OS_FORCE_IPV4=true
# source openrc file from OVH
ansible-playbook provision-zabbix.yml
# Now place DNS entries in cloudflare before proceeding
ansible-playbook -i ../inventories/openstack_inventory.py deploy-galera.yml
ansible-playbook -i ../inventories/openstack_inventory.py deploy-zabbix.yml
```

To run elsewhere you must create your servers, define your inventory, and then
run the deploy-galera and deploy-zabbix playbooks
