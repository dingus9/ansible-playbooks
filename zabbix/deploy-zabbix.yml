---
- name: Generate SSL certificates for Zabbix
  hosts: meta-type_zabbix
  user: centos
  become: True
  vars:
    cert_name: test_selfsigned
    tmp_run_dir: "/tmp"
  pre_tasks:
    - name: Install EPEL
      package:
        name: epel-release
        state: present

    - name: Ensure pip is install
      package:
        name: python-pip
        state: present

    - block:
        - name: Create temporary dir for run
          file:
            path: "{{ molecule_run_dir }}/tmp"
            state: directory
          register: cert_tmpdir

        - name: Create CA key
          community.crypto.openssl_privatekey:
            path: "{{ cert_tmpdir.path }}/CA_key.pem"
          register: ca_key

        - name: Create the CA CSR
          community.crypto.openssl_csr:
            path: "{{ cert_tmpdir.path }}/CA.csr"
            privatekey_path: "{{ ca_key.filename }}"
            basic_constraints:
              - CA:TRUE
            common_name: "my-ca"
          register: ca_csr

        - name: sign the CA CSR
          community.crypto.x509_certificate:
            path: "{{ cert_tmpdir.path }}/CA.crt"
            csr_path: "{{ ca_csr.filename }}"
            privatekey_path: "{{ ca_key.filename }}"
            provider: selfsigned
          register: ca_crt

        - name: Generate an OpenSSL private key with the default values (4096 bits, RSA)
          community.crypto.openssl_privatekey:
            path: "{{ cert_tmpdir.path }}/{{ cert_name }}.pem"
          register: test_key

        - name: Generate an OpenSSL Certificate Signing Request
          community.crypto.openssl_csr:
            path: "{{ cert_tmpdir.path }}/{{ cert_name }}.csr"
            privatekey_path: "{{ test_key.filename }}"
            common_name: graylog.example.com
            subject_alt_name:
              - "IP:{{ hostvars[inventory_hostname]['ansible_eth0']['ipv4']['address'] }}"
              - "DNS:zabbix.dldev.xyz"
              - "DNS:zabbix.dev.dldev.xyz"
              - "IP:127.0.0.1"
              - "DNS:localhost"
          register: test_csr

        - name: Generate a Self Signed OpenSSL certificate
          community.crypto.x509_certificate:
            path: "{{ cert_tmpdir.path }}/{{ cert_name }}.crt"
            privatekey_path: "{{ test_key.filename }}"
            csr_path: "{{ test_csr.filename }}"
            ownca_path: "{{ ca_crt.filename }}"
            ownca_privatekey_path: "{{ ca_key.filename }}"
            provider: ownca
          register: test_crt

      run_once: true
      become: false
      delegate_to: localhost

    - name: Copy openSSL files
      copy:
        src: "{{ item.file }}"
        dest: "{{ item.dest }}"
      loop:
        - dest: "/etc/pki/tls/certs/{{ test_crt.filename | basename }}"
          file: "{{ test_crt.filename }}"
        - dest: "/etc/pki/tls/private/{{ test_key.filename | basename }}"
          file: "{{ test_key.filename }}"
        - dest: "/etc/pki/ca-trust/source/anchors/{{ ca_crt.filename | basename }}"
          file: "{{ ca_crt.filename }}"

    - name: Update ca trust
      command: |
        update-ca-trust extract
        update-ca-trust enable
      changed_when: False
      become: True

- name: Deploy Zabbix Server
  hosts: meta-type_zabbix
  user: centos
  become: true
  gather_facts: true
  vars:
    zabbix_vip: 172.22.5.250
  tags:
    - zabbix_server
  pre_tasks:

    - name: Set backend_nodes temporary fact
      set_fact:
        backend_nodes_tmp:
          name: "{{ item }}"
          address: "{{ hostvars[item]['ansible_eth1']['ipv4']['address'] }}"
          port: 3306
      loop: "{{ groups['meta-type_zabbix'] }}"
      register: tmp_backend_nodes

    - name: Set backend_nodes fact
      set_fact:
        backend_nodes: "{{ tmp_backend_nodes.results | map(attribute='ansible_facts.backend_nodes_tmp') | list }}"

  roles:
    - role: ansible_role_zabbix_server
      zabbix_server_apache_servername: zabbix.dldev.xyz
      zabbix_server_sourceip: "{{ zabbix_vip }}"
      zabbix_server_url_aliases:
        - localhost
      zabbix_server_database_interface: eth1
      zabbix_server_ldap_enable: True
      zabbix_server_ldap_bind_dn: "cn=admin,dc=example,dc=org"
      zabbix_server_ldap_bind_password: "{{ lookup('env', 'ZABBIX_SERVER_LDAP_BIND_PASSWORD') }}"
      zabbix_server_ldap_base_dn: "ou=users,dc=example,dc=org"
      zabbix_server_ldap_search_attribute: "uid"
      zabbix_server_ldap_port: 389
      zabbix_server_ldap_host: 172.22.5.175
      zabbix_server_apache_tls: True
      zabbix_server_apache_redirect: True
      zabbix_server_apache_tls_crt: "/etc/pki/tls/certs/{{ test_crt.filename | basename }}"
      zabbix_server_apache_tls_key: "/etc/pki/tls/private/{{ test_key.filename | basename }}"
      zabbix_apache_vhost_tls_port: 443
      zabbix_server_dbhostname: "{{ groups['meta-type_zabbix'][0] }}"
      zabbix_server_dbuser: zbx
      zabbix_server_dbport: 3306
      zabbix_server_dbpassword: "{{ lookup('env', 'ZABBIX_SERVER_DBPASSWORD') }}"
      zabbix_database_user_allowed_host: localhost
      zabbix_server_dbname: zabbix-server
      zabbix_server_web_password: zabbix
      zabbix_server_mysql_login_host: localhost
      zabbix_server_mysql_login_user: root
      zabbix_server_mysql_login_password: test
      zabbix_server_version: 5.0.8

    - role: ansible_role_ha
      tags:
        - ha
      vars:
        ha_manage_hosts: true
        ha_cluster_password: changeme
        ha_cluster_password_salt: 'SDFjsdlkajfJOIDFklkj**UO#$kllkfksd'
        ha_cluster_nodes: "{% set ips = [] %}{% for hostname in groups['meta-type_zabbix'] %}{{ips.append({'name': hostname,'ip':hostvars[hostname]['ansible_eth0']['ipv4']['address']})}}{% endfor %}{{ ips }}"
        ha_clusters:
          - name: zabbix-ha
            nodes: "{{ groups['meta-type_zabbix'] }}"
            properties:
              - name: "stonith-enabled"
                value: "false"
              - name: "no-quorum-policy"
                value: "ignore"
            resources:
              - name: vip
                type: ocf:heartbeat:IPaddr2
                options:
                  nic: eth0
                  ip: "{{ zabbix_vip }}"
                  cidr_netmask: 24
                monitor_interval: 20
              - name: httpd
                type: systemd:httpd
                monitor_interval: 60
              - name: zabbix-server
                type: systemd:zabbix-server
                monitor_interval: 60
            groups:
              - name: zabbix-grp
                resource_members:
                  - httpd
                  - zabbix-server
            constraints:
              - name: colocation
                options:
                  type: add
                  resources:
                    - zabbix-grp
                    - vip
                  score: INFINITY
              - name: order
                options:
                  - resource: vip
                    action: start
                  - resource: zabbix-grp
                    action: start

    - role: ansible_role_zabbix_agent
      zabbix_agent_server: zabbix.dldev.xyz
      zabbix_agent_serveractive: zabbix.dldev.xyz
      zabbix_server_url: https://zabbix.dldev.xyz
      zabbix_agent_ip: "{{ hostvars[inventory_hostname]['ansible_eth0']['ipv4']['address'] }}"
      zabbix_agent_version: "5.0.8"
      zabbix_api_user: Admin
      zabbix_api_pass: zabbix
      tags:
        - zabbix_agent
